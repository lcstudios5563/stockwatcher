const fs = require('fs');
const path = require('path');
const request = require("request");
const cheerio = require("cheerio")
const cheerioTableparser = require('cheerio-tableparser');;
const loader = require('csv-load-sync');

module.exports = {
	////downloadDivData:
	//getDivData
	scrape: function(req, res){

		var tickers = new Array();	
		var markets = new Array();	
		var divs = new Array();
		//var decDates = new Array();
		var exDivDates = new Array();
		var payDates = new Array();

		request({
			url: "http://www.dividenddata.co.uk/",
            headers: {
                'User-Agent': 'Mozilla/5.0'
            },
		}, function(error, response, body) {
			var $ = cheerio.load(body);
			cheerioTableparser($);
			var data = $(".table-striped").parsetable(false, false, false);
			
			for(var i = 0; i < data.length; i++) {
				var innerData = data[i];
				for(var j = 0; j < innerData.length; j++) {		
					if(i==0 && j>0){
						var ticker = innerData[j]
						if( ticker.indexOf('.') > -1) ticker = ticker.substring(0, ticker.indexOf('.'));						
						ticker = ticker + '.L'
						tickers.push(ticker);
						//console.log(ticker);	
					}
					if(i==2 && j>0) markets.push(innerData[j]);
					if(i==4 && j>0) divs.push(innerData[j]);
					//if(i==6 && j>0) decDates.push(innerData[j]);
					if(i==7 && j>0) exDivDates.push(innerData[j]);
					if(i==8 && j>0) payDates.push(innerData[j]);
   				 }
			}

			var file = fs.createWriteStream('./data/DivDates.csv');
		//file.on('error', function(err) { /* error handling */ });
			file.write('Ticker,Market,Div,ExDivDate,PayDate\r\n')
			for(var i = 0; i < tickers.length; i++) {
				file.write(tickers[i]+','+markets[i]+','+divs[i]+','+
					exDivDates[i]+','+payDates[i]+'\r\n')
			}
			file.end();

			//console.log('hello world');	
		});	//request 
		
		res.send('hello worldaaa');
							
	},//scrapeFunc

	get: function(req, res){
		var csv = loader('./data/DivDates.csv');
		var json = JSON.stringify(csv)
		res.send(json);
	}
}//exports