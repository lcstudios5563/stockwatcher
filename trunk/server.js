var express = require('express');
var basicAuth = require('express-basic-auth')
var bodyParser = require('body-parser')
var cors = require('cors');
var app = express();

app.use(cors()); 

app.get('/', function (req, res) {
//    res.send('Hello World');
    res.sendFile(__dirname + '/views/index.html');
   
})

var routes = require(__dirname+'/routes.js');
app.use('/api', routes);


var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("App listening at http://%s:%s", host, port)
})