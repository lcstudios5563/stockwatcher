var express = require('express');
var basicAuth = require('express-basic-auth')
var app = express();
var router = express.Router();

//var profCtrl = require('./routes/profileController');
var ftseDataCtrl = require( __dirname + '/routes/FtseData.js');
var ftseDivCtrl = require( __dirname + '/routes/FtseDiv.js');

//routes not needing auth
router.route('/FtseDivGet').get(ftseDivCtrl.get);
//routes needing auth
 router.use(basicAuth({
     users: { 'admin': 'supersecret' }
 }))
//router.route('/FtseDataScrape').get(ftseDataCtrl.scrape);
router.route('/FtseDivScrape').get(ftseDivCtrl.scrape);
//http://localhost:8081/api/profile

module.exports = router;
